import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Layout, Menu } from 'antd';

import RoomMenu from './containers/roomMenu';
import RoomContent from './containers/roomContent';

class App extends React.Component {
  render() {
    return (
      <Layout>
        <Layout.Header>
          <Menu theme="dark" mode="horizontal" style={{ lineHeight: '64px' }} />
        </Layout.Header>
        <Layout.Content style={{ padding: '0 50px' }}>
          <Layout style={{ marginTop: '50px', padding: '24px 0', background: '#fff' }}>
            <Layout.Sider>
              <RoomMenu />
            </Layout.Sider>
            <Layout.Content
              style={{ padding: '0 24px', minHeight: 280, maxHeight: 500 }}>
              <Switch>
                <Route exact path="/" render={() => 'Select room from left menu'} />
                <Route path="/:room" component={RoomContent} />
              </Switch>
            </Layout.Content>
          </Layout>
        </Layout.Content>
        <Layout.Footer>
          profiq Chat demo for Move Fast meetup ©2018 created by Filip Vavera
        </Layout.Footer>
      </Layout>
    );
  }
}

export default App;
