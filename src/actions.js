import { makeActionCreator } from 'redux-toolbelt';

export const selectRoom = makeActionCreator('SELECT_ROOM');
