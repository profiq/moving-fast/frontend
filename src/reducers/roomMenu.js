import { selectRoom } from '../actions';

const initialState = {
  selected: null,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case selectRoom.TYPE:
      return { ...state, selected: payload };
    default:
      return state;
  }
};
