import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { createFlagsReducer } from 'flag';

// Flags
import flagsConfig from '../flags';
import roomMenuReducer from './roomMenu';

export default (history) =>
  combineReducers({
    roomMenu: roomMenuReducer,

    // Flags
    flags: createFlagsReducer(flagsConfig),
    // Router
    router: connectRouter(history),
  });
